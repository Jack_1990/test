'use strict';


describe('ctrl', function () {

    var $controller;

    // Setup for all tests
    beforeEach(function () {
        // loads the app module
        module('strApp');
        inject(function (_$controller_) {
            // inject removes the underscores and finds the $controller Provider
            $controller = _$controller_;
        });
    });


    describe('$scope', function () {
        it('first table test', function () {

            var $scope = {};
            // $controller takes an object containing a reference to the $scope
            var controller = $controller('ctrl', {$scope: $scope});

            $scope.lengthBox = 8530;
            $scope.lengthFromWheel = 605;
            $scope.wheelBase = 5960;
            $scope.readDistr = 3015;


            $scope.calc1();
            expect($scope.tbl1.LFW).toEqual(605);
            expect($scope.tbl1.WB).toEqual(5960);
            expect($scope.tbl1.LB).toEqual(8530);
            expect($scope.tbl1.RD).toEqual(3015);
            expect($scope.tbl1.MC).toEqual(1090);
            expect($scope.tbl1.L1).toEqual(3175);
        });

        // it('second table test', function () {
        //
        //     var $scope = {};
        //     // $controller takes an object containing a reference to the $scope
        //     var controller = $controller('ctrl', {$scope: $scope});
        //
        //     $scope.lengthBox = 8530;
        //     $scope.lengthFromWheel = 605;
        //     $scope.wheelBase = 5960;
        //     $scope.readDistr = 3015;
        //
        //     $scope.calc2();
        //
        //     expect($scope.tbl1.LFW).toEqual(605);
        //     expect($scope.tbl1.WB).toEqual(5960);
        //     expect($scope.tbl1.LB).toEqual(8530);
        //     expect($scope.tbl1.RD).toEqual(3015);
        //     expect($scope.tbl1.MC).toEqual(1090);
        //     expect($scope.tbl1.L1).toEqual(3175);
        // });
    });
});