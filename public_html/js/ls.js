/**
 * Created by user on 30/07/2016.
 */

const dofenAll = [
    {name: 'None דופן', mass: 0., carrying: 0., underCar: false},
    {name: 'AL 750', mass: 270., carrying: 750., underCar: false},
    {name: 'AL 1000', mass: 360., carrying: 1000., underCar: false},
    {name: 'AL 1500', mass: 430., carrying: 1500., underCar: false},
    {name: 'AL 2000', mass: 450., carrying: 2000., underCar: false},
    {name: 'ST 1500', mass: 650., carrying: 1500., underCar: false},
    {name: 'ST 2000', mass: 650., carrying: 2000., underCar: false},
    {name: 'מתכנסת', mass: 625., carrying: 0., underCar: true}
];

const kirurAll = [
    {name: "None יח'' קירור", mass: 0., underCar: false},
    {name: "Variable", mass: 0., underCar: false, editable: true},
    {name: 'על המנוע', mass: 70., underCar: true},
    {name: 'Carrier 450', mass: 380., underCar: false},
    {name: 'Carrier 550', mass: 470., underCar: false},
    {name: 'Carrier 950', mass: 650., underCar: false},
    {name: 'Carrier 750', mass: 480., underCar: false},
    {name: 'Carrier 850', mass: 500., underCar: false},
    {name: 'Carrier 1150', mass: 620., underCar: false},
    {name: 'Carrier 1250', mass: 680., underCar: false},
    {name: 'ThermoKing T-1000', mass: 500., underCar: false},
    {name: 'Lummico', mass: 100., underCar: false},
    {name: 'HT50', mass: 570., underCar: false},
    {name: 'HT70', mass: 660., underCar: false},
    {name: 'HT30', mass: 460., underCar: false},
    {name: 'HT250', mass: 426., underCar: false},
    {name: 'HT150', mass: 416., underCar: false},
    {name: 'HT500', mass: 80., underCar: false},
    {name: 'TR-600S', mass: 450., underCar: false},
    {name: 'TR-1000S', mass: 560., underCar: false},
    {name: 'A-Frost 700', mass: 80., underCar: false},
    {name: 'A-Frost 900', mass: 80., underCar: false},
    {name: 'A-Frost 1100', mass: 80., underCar: false},
    {name: 'Aklim 8000', mass: 80., underCar: false},
    {name: 'ThermoKing T-800', mass: 477., underCar: false}	
];

const cabineAll = [
    {name: 'נהג + 1', humans: 2, L: 0., editable: false},
    {name: 'נהג + 2', humans: 3, L: 0., editable: false},
    {name: 'נהג + 6', humans: 7, L: 0., editable: false},
    {name: 'נהג + X', humans: 0, L: 0., editable: true}
];

const humanWeight = 75.;

const BASE_IMG_PATH = "assets/images/";


function cabinCalc(humanNumber, wheelBase, L) {
    let sum = humanWeight * humanNumber;
    let mBack = sum * L / wheelBase;
    let mFront = sum - mBack;
    return {
        mBack: mBack,
        mFront: mFront
    };
}


angular.module('strApp', ['pascalprecht.translate', 'ngSanitize', 'ngCookies'])
    .config(['$translateProvider', function ($translateProvider) {

        //default language
        $translateProvider.preferredLanguage('en');
        //fallback language if entry is not found in current language
        $translateProvider.fallbackLanguage('en');
        //load language entries from files
        $translateProvider.useStaticFilesLoader({
            prefix: '/assets/languages/', //relative path Eg: /assets/languages/
            suffix: '.json' //file extension
        });

        $translateProvider.useSanitizeValueStrategy('sanitize');
        $translateProvider.useCookieStorage();

    }])
    .run(function ($rootScope, Language) {
        //make the service available
        $rootScope.Language = Language;
    })
    .directive('focus', function () {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                elem.bind('keydown', function (e) {
                    var code = e.keyCode || e.which;
                    if (code === 13) {
                        try {
                            //alert(attrs.tabindex);
                            if (attrs.tabindex !== undefined) {
                                var nextTabIndex = parseInt(attrs.tabindex) + 1;
                                $("[tabindex=" + nextTabIndex + "]").focus();
                            }
                        } catch (e) {

                        }
                    }
                });
            }
        }
    })
    .factory('Language', function ($translate) {
        //add the languages you support here.
        var rtlLanguages = ['he'];

        var isRtl = function () {
            var languageKey = $translate.proposedLanguage() || $translate.use();
            for (var i = 0; i < rtlLanguages.length; i += 1) {
                if (languageKey.indexOf(rtlLanguages[i]) > -1)
                    return true;
            }
            return false;
        };

        //public api
        return {
            isRtl: isRtl
        };
    })
    .controller('ctrl', function ($scope, $translate) {

        $scope.today = new Date();

        $scope.changeLanguage = function () {
            //use parameter needs to be part of a known locale Eg: en-UK, en, etc
            $translate.use($scope.selectedLanguage);
        };

        $scope.tbl1 = {
            LFW: '',
            WB: '',
            LB: '',
            MC: '',
            L1: '',
            RD: ''
        };

        //if value is more than allowed
        $scope.validateMass = function () {
            $scope.summBackError = $scope.licBack < $scope.tbl2.summCalc.back;
            $scope.summMassError = $scope.licAll < $scope.tbl2.summCalc.massSum;
            $scope.summFrontError = $scope.licFront < $scope.tbl2.summCalc.front;
            $scope.degemMassError = $scope.degemMass !== ($scope.degemBack + $scope.degemFront);
            $scope.degemMassBackError = $scope.degemBack >= $scope.degemFront;
            $scope.licBackError = $scope.licBack < $scope.licFront;
        };

        $scope.validate06Wb = function () {

            //if kirur+dofen < 300
            $scope.wb06Error = $scope.kirurSel.mass > 0 && $scope.dofenSel.mass > 0 && (($scope.tbl1.WB * 0.6 - $scope.tbl1.L1) < 300);

            //if on kirur + dofen < 220
            if ($scope.kirurSel.mass === 0. && $scope.dofenSel.mass > 0 && (($scope.tbl1.WB * 0.6 - $scope.tbl1.L1) < 220)) {
                $scope.wb06Error = true;
            }
            //if no kirur + no dofen <60
            if ($scope.kirurSel.mass === 0. && $scope.dofenSel.mass === 0. && (($scope.tbl1.WB * 0.6 - $scope.tbl1.L1) < 60)) {
                $scope.wb06Error = true;
            }

            //initial failure
            if ($scope.tbl1.WB * 0.6 - $scope.tbl1.L1 === 0) {
                $scope.wb06Error = false;
            }
        };


        $scope.dofens = dofenAll;
        $scope.kirurs = kirurAll;
        $scope.cabines = cabineAll;

        $scope.cabineSel = cabineAll[1];
        $scope.dofenSel = dofenAll[0];
        $scope.ВSel = kirurAll[0];

        $scope.selectedLanguage = $translate.use(); //default


        $scope.img_src = BASE_IMG_PATH + "l1-n1-no-kirur-no-dofen.jpg";


        $scope.selectPic = function () {

            let dofen = $scope.dofenSel.mass;
            let kirur = $scope.kirurSel.mass;
            let l1 = $scope.tbl1.L1;
            let N = $scope.readDistr;


            if (l1 >= N) {
                if (dofen > 0.) {
                    if($scope.dofenSel.underCar===true){
                        if (kirur > 0.) {
                            if ($scope.kirurSel.underCar === true) {
                                $scope.img_src = BASE_IMG_PATH + "l1-n1-kirur-undercar-dofen-undercar.jpg";
                            } else {
                                $scope.img_src = BASE_IMG_PATH + "l1-n1-kirur-dofen-undercar.jpg";
                            }
                        } else {
                            $scope.img_src = BASE_IMG_PATH + "l1-n1-no-kirur-dofen-undercar.jpg";
                        }
                    }else {
                        if (kirur > 0.) {
                            if ($scope.kirurSel.underCar === true) {
                                $scope.img_src = BASE_IMG_PATH + "l1-n1-kirur-undercar-dofen.jpg";
                            } else {
                                $scope.img_src = BASE_IMG_PATH + "l1-n1-kirur-dofen.jpg";
                            }
                        } else {
                            $scope.img_src = BASE_IMG_PATH + "l1-n1-no-kirur-dofen.jpg";
                        }
                    }

                } else {
                    if (kirur > 0.) {
                        if ($scope.kirurSel.underCar === true) {
                            $scope.img_src = BASE_IMG_PATH + "l1-n1-kirur-undercar-no-dofen.jpg";
                        } else {
                            $scope.img_src = BASE_IMG_PATH + "l1-n1-kirur-no-dofen.jpg";
                        }
                    } else {
                        $scope.img_src = BASE_IMG_PATH + "l1-n1-no-kirur-no-dofen.jpg";
                    }
                }
            } else {
                if (dofen > 0.) {
                    if($scope.dofenSel.underCar===true){
                        if (kirur > 0.) {
                            if ($scope.kirurSel.underCar === true) {
                                $scope.img_src = BASE_IMG_PATH + "n1-l1-kirur-undercar-dofen-undercar.jpg";
                            } else {
                                $scope.img_src = BASE_IMG_PATH + "n1-l1-kirur-dofen-undercar.jpg";
                            }
                        } else {
                            $scope.img_src = BASE_IMG_PATH + "n1-l1-no-kirur-dofen-undercar.jpg";
                        }
                    }else {
                        if (kirur > 0.) {
                            if ($scope.kirurSel.underCar === true) {
                                $scope.img_src = BASE_IMG_PATH + "n1-l1-kirur-undercar-dofen.jpg";
                            } else {
                                $scope.img_src = BASE_IMG_PATH + "n1-l1-kirur-dofen.jpg";
                            }
                        } else {
                            $scope.img_src = BASE_IMG_PATH + "n1-l1-no-kirur-dofen.jpg";
                        }
                    }

                } else {
                    if (kirur > 0.) {
                        if ($scope.kirurSel.underCar === true) {
                            $scope.img_src = BASE_IMG_PATH + "n1-l1-kirur-undercar-no-dofen.jpg";
                        } else {
                            $scope.img_src = BASE_IMG_PATH + "n1-l1-kirur-no-dofen.jpg";
                        }
                    } else {
                        $scope.img_src = BASE_IMG_PATH + "n1-l1-no-kirur-no-dofen.jpg";
                    }
                }
            }
        }
        $scope.calc1 = function () {
            let mc = function () {
                return $scope.wheelBase - $scope.lengthFromWheel - $scope.lengthBox / 2;
            };
            let l1 = function () {
                return $scope.lengthFromWheel + $scope.lengthBox - $scope.wheelBase;
            };

            $scope.tbl1 = {
                LFW: $scope.lengthFromWheel,
                WB: $scope.wheelBase,
                LB: $scope.lengthBox,
                MC: mc(),
                L1: l1(),
                RD: $scope.readDistr
            };

            $scope.calc2();
            $scope.validate06Wb();
            $scope.selectPic();
        };


        $scope.calc2 = function () {
            let cirback = function () {
                return $scope.kirurSel.mass * ($scope.tbl1.LFW - 210) / $scope.tbl1.WB
            };

            let dofMass = function (dofen) {
                if (dofen.carrying > 0 && dofen.carrying <= 1000) {
                    return 60;
                }
                if (dofen.carrying > 1000 && dofen.carrying <= 1500) {
                    return 178;
                }
                return 0;
            };
            let dofback = function () {
                if ($scope.dofenSel.underCar === true) {
                    return $scope.dofenSel.mass * ($scope.tbl1.WB + $scope.tbl1.L1 - 1030) / $scope.tbl1.WB;
                } else {
                    return $scope.dofenSel.mass * ($scope.tbl1.L1 + $scope.tbl1.WB - dofMass($scope.dofenSel)) / $scope.tbl1.WB;
                }

            };

            let cabineMass = cabinCalc($scope.cabineSel.humans, $scope.tbl1.WB, $scope.cabineSel.L);

            $scope.tbl2 = {
                cabinCalc: {
                    name: $scope.cabineSel.name,
                    massSum: cabineMass.mFront + cabineMass.mBack,
                    front: cabineMass.mFront,
                    back: cabineMass.mBack
                },
                cirurCalc: {
                    name: $scope.kirurSel.name,
                    massSum: $scope.kirurSel.mass,
                    back: cirback(),
                    front: $scope.kirurSel.mass - cirback()
                },
                dofenCalc: {
                    name: $scope.dofenSel.name,
                    massSum: $scope.dofenSel.mass,
                    back: dofback(),
                    front: $scope.dofenSel.mass - dofback()
                }
            };
            let carSum = function () {
                return $scope.licAll - $scope.tbl2.cirurCalc.massSum - $scope.tbl2.dofenCalc.massSum - $scope.tbl2.cabinCalc.massSum - $scope.degemMass
            };
            const carback = function () {
                return carSum() * ($scope.tbl1.WB - $scope.tbl1.MC) / $scope.tbl1.WB
            };
            const carfront = function () {
                return carSum() - carback()
            };
            $scope.tbl2.cargoCalc = {
                name: "מטען",
                massSum: carSum(),
                back: carback(),
                front: carfront()
            };
            $scope.tbl2.summCalc = {
                name: "סה''כ מחישוב",
                massSum: $scope.licAll,
                front: $scope.degemFront + $scope.tbl2.cabinCalc.front + $scope.tbl2.cirurCalc.front + $scope.tbl2.dofenCalc.front + $scope.tbl2.cargoCalc.front,
                back: $scope.degemBack + $scope.tbl2.cabinCalc.back + $scope.tbl2.cirurCalc.back + $scope.tbl2.dofenCalc.back + $scope.tbl2.cargoCalc.back
            };

            $scope.validateMass();
            $scope.validate06Wb();
            $scope.selectPic();
        };
    });
